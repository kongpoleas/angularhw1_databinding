import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {
  lightOff = 'assets/pic/off.png'
  lightOn = 'assets/pic/on.png'

  isOnButton = true;
  isLightOn = true;
  value: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  onToggleLight() {
    this.isLightOn = !this.isLightOn;
    this.isOnButton = !this.isOnButton;
  }

}
